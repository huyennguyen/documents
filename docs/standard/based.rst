.. Created by Rubikin Team.
.. ========================
.. Date: 2014-06-15
.. Time: 03:27:05 AM

.. Author: Vinh Trinh <vinhtrinh@rubikin.com>

.. This file is part of Rubikin

.. Question? Come to our website at http://rubikin.com
.. For the full copyright and license information, please view the LICENSE
.. file that was distributed with this source code.


Based Coding Standard
=====================

This document contains the based coding standard and guidelines for every **Rubikin** and **Nilead** projects.

This document is the definite guideline for our developers on how they **MUST** write their code. Instead of coding in your own preferred style, you **MUST** write all code to the standards outlined in this document. This makes sure that every our projects are coded in a consistent style - parts are not written differently by different programmers.

Not only does this solution make the code easier to understand, it also ensures that any developer who looks at the code will know what to expect throughout the entire application.

Our standards based on `PSR Coding Standard <http://www.php-fig.org/>`_ for **PHP**, `Google Coding Style Guide <http://google-styleguide.googlecode.com/svn/trunk/>`_ for other languagues. All these standards or guild lines may be overridden by this document or other standard documents published by `Rubikin <http://rubikin.com/>`__.

This document may be extended and overridden by other **Coding Standard** published by `Rubikin <http://rubikin.com/>`__'s masters for secluded case.


Overview
--------

- Files **MUST** use only UTF-8 without BOM.
- Files extension **MUST** use only lowercase.
- Files **MUST** use the Unix LF *(linefeed)* line ending.
- Files **MUST** end with a single blank line.
- There **MUST NOT** be trailing whitespace at the end of non-blank lines.
- There **MUST NOT** be more than one statement per line.
- Code **MUST** use 4 spaces for indenting, NOT tabs.


Files Encoding
--------------

**Uses UTF-8 (without BOM)**

Make sure your editor uses UTF-8 as character encoding, without a BOM *(Byte Order Mark)* character.


Files Extension
---------------

**Use lowercase**

Files extension **MUST** use only lowercase includes images and any resource files *(\*.php, \*.html, \*.css, \*.jpg, \*.ttf, etc...).*


Lines
-----

**Use the Unix LF (linefeed) for line ending**

All files **MUST** use the Unix LF *(linefeed)* line ending. NOT use CR *(carriage returns)* as old Macintoshs or CRLF *(carriage return - line feed combination)* as Windows.

All files **MUST** end with a single blank line.

There **MUST NOT** be trailing whitespace at the end of non-blank lines.

There **MUST NOT** be more than one statement per line.


Indentation
-----------

**Use 4 spaces**

Code **MUST** use 4 spaces for indenting, NOT tabs.

.. code-block:: java

    for (int i = 1; i <= 10; i++) {
        System.out.println('Line ' + i);
    }


Documentation
-------------

Every source code files **MUST** add file comment.

For each syntax, the files comment are different BUT it **MUST** have this line:

.. code-block:: html

    This file is part of Rubikin

This is PHP file comment example:

.. code-block:: php

    <?php
    /**
     * Created by Rubikin Team.
     * ========================
     * Date: 2014-06-17
     * Time: 04:03:53 AM
     *
     * @author Author Name <author@email.address>
     *
     * This file is part of Rubikin
     *
     * Question? Come to our website at http://rubikin.com
     * For the full copyright and license information, please view the LICENSE
     * file that was distributed with this source code.
     */

    // Your code goes here...

And this is for TWIG template:

.. code-block:: html

    <!doctype html>

    {#
    Created by Rubikin Team.
    ========================
    Date: 2014-06-17
    Time: 04:06:42 AM

    Author: Author Name <author@email.address>

    Contributors:
        Contributer 1 <contributer1@email.address>
        Contributer 2 <contributer2@email.address>

    This file is part of Rubikin

    Question? Come to our website at http://rubikin.com
    #}
