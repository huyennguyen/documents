
http://learn.shayhowe.com/html-css/writing-your-best-code/
http://csswizardry.com/2012/04/my-html-css-coding-style/

==========================
HTML / CSS Coding Standard
==========================

*This document defines formatting and style rules for HTML and CSS. It aims at improving collaboration, code quality, and enabling supporting infrastructure. It applies to raw, working files that use HTML and CSS, including GSS files. Tools are free to obfuscate, minify, and compile as long as the general code quality is maintained.*

General Style Rule
==================

Protocol
--------

Omit the protocol from embedded resources.

Omit the protocol portion (``http:``, ``https:``) from URLs pointing to images and other media files, style sheets, and scripts unless the respective files are not available over both protocols.

Omitting the protocol—which makes the URL relative—prevents mixed content issues and results in minor file size savings.

.. code-block:: html

    <!-- Not recommended -->
    <script src="http://www.google.com/js/gweb/analytics/autotrack.js"></script>

.. code-block:: html

    <!-- Recommended -->
    <script src="//www.google.com/js/gweb/analytics/autotrack.js"></script>

.. code-block:: css

    /* Not recommended */
    .example {
        background: url(http://www.google.com/images/example);
    }

.. code-block:: css

    /* Recommended */
    .example {
        background: url(//www.google.com/images/example);
    }

General Formatting Rules
========================

Indentation
-----------

Indent by 4 spaces at a time. DO NOT use tabs or mix tabs and spaces for indentation.

.. code-block:: html

    <ul>
        <li>Fantastic</li>
        <li>Great</li>
    </ul>

.. code-block:: css

    .example {
        color: blue;
    }

Capitalization
--------------

Use only lowercase.
All code has to be lowercase: This applies to HTML element names, attributes, attribute values (unless text/CDATA), CSS selectors, properties, and property values (with the exception of strings).

.. code-block:: html

    <!-- Not recommended -->
    <A HREF="/">Home</A>

.. code-block:: html

    <!-- Recommended -->
    <img src="google.png" alt="Google">

.. code-block:: css

    /* Not recommended */
    color: #E5E5E5;

.. code-block:: css

    /* Recommended */
    color: #e5e5e5;
    Trailing Whitespace

Remove trailing white spaces.
Trailing white spaces are unnecessary and can complicate diffs.

.. code-block:: html

    <!-- Not recommended -->
    <p>What?_


.. code-block:: html

    <!-- Recommended -->
    <p>Yes please.</p>


General Meta Rules
==================

Encoding
--------

Use UTF-8 (without BOM).

Make sure your editor uses UTF-8 as character encoding, without a Byte Order Mark.

Specify the encoding in HTML templates and documents via ``<meta charset="utf-8">``. Do not specify the encoding of style sheets as these assume UTF-8.

*(More on encodings and when and how to specify them can be found in Handling character encodings in HTML and CSS.)*

Comments
--------

Explain code as needed, where possible.
Use comments to explain code: What does it cover, what purpose does it serve, why is respective solution used or preferred?

*(This item is optional as it is not deemed a realistic expectation to always demand fully documented code. Mileage may vary heavily for HTML and CSS code and depends on the project’s complexity.)*

Action Items
------------

Mark todos and action items with ``TODO``.
Highlight todos by using the keyword ``TODO`` only, not other common formats like ``@@``.

Append a contact (username or mailing list) in parentheses as with the format ``TODO`` (contact).

Append action items after a colon as in ``TODO``: action item.

.. code-block:: html

    {# TODO (john.doe): revisit centering #}
    <center>Test</center>

    <!-- TODO: remove optional tags -->
    <ul>
        <li>Apples</li>
        <li>Oranges</li>
    </ul>

HTML Style Rules
================

Document Type
-------------

Use HTML5.
HTML5 (HTML syntax) is preferred for all HTML documents: ``<!DOCTYPE html>``.

*(It’s recommended to use ``HTML``, as ``text/html``. Do not use ``XHTML``. ``XHTML``, as ``application/xhtml+xml``, lacks both browser and infrastructure support and offers less room for optimization than HTML.)*

Although fine with HTML, do not close void elements, i.e. write ``<br>``, not ``<br />``.

HTML Validity
-------------

Use valid HTML where possible.
Use valid HTML code unless that is not possible due to otherwise unattainable performance goals regarding file size.

Use tools such as the `W3C HTML <http://validator.w3.org/>`_ validator to test.

Using valid HTML is a measurable baseline quality attribute that contributes to learning about technical requirements and constraints, and that ensures proper HTML usage.

.. code-block:: html

    <!-- Not recommended -->
    <title>Test</title>
    <article>This is only a test.

.. code-block:: html

    <!-- Recommended -->
    <!doctype html>

    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Document</title>
    </head>

    <body>
        <h1>This is a heading level 1</h1>
    </body>
    </html>

Semantics
---------

Use HTML according to its purpose.
Use elements (sometimes incorrectly called “tags”) for what they have been created for. For example, use heading (``h1`` to ``h6``) elements for headings, ``p`` elements for paragraphs, ``a`` elements for anchors, etc...

Using HTML according to its purpose is important for accessibility, reuse, and code efficiency reasons.

.. code-block:: html

    <!-- Not recommended -->
    <div onclick="goToRecommendations();">All recommendations</div>

.. code-block:: html

    <!-- Recommended -->
    <a href="recommendations/">All recommendations</a>

Multimedia Fallback
-------------------

Provide alternative contents for multimedia.
For multimedia, such as images, videos, animated objects via canvas, make sure to offer alternative access. For images that means use of meaningful alternative text (``alt``) and for video and audio transcripts and captions, if available.

Providing alternative contents is important for accessibility reasons: A blind user has few cues to tell what an image is about without @alt, and other users may have no way of understanding what video or audio contents are about either.

(For images whose ``alt`` attributes would introduce redundancy, and for images whose purpose is purely decorative which you cannot immediately use CSS for, use no alternative text, as in ``alt=""``.)

.. code-block:: html

    <!-- Not recommended -->
    <img src="spreadsheet.png">

.. code-block:: html

    <!-- Recommended -->
    <img src="spreadsheet.png" alt="Spreadsheet screenshot.">

Separation of Concerns
----------------------

Separate structure from presentation from behavior.
Strictly keep structure (markup), presentation (styling), and behavior (scripting) apart, and try to keep the interaction between the three to an absolute minimum.

That is, make sure documents and templates contain only HTML and HTML that is solely serving structural purposes. Move everything presentational into style sheets, and everything behavioral into scripts.

In addition, keep the contact area as small as possible by linking as few style sheets and scripts as possible from documents and templates.

Separating structure from presentation from behavior is important for maintenance reasons. It is always more expensive to change HTML documents and templates than it is to update style sheets and scripts.

.. code-block:: html

    <!-- Not recommended -->
    <!DOCTYPE html>
    <title>HTML sucks</title>
    <link rel="stylesheet" href="base.css" media="screen">
    <link rel="stylesheet" href="grid.css" media="screen">
    <link rel="stylesheet" href="print.css" media="print">
    <h1 style="font-size: 1em;">HTML sucks</h1>
    <p>I’ve read about this on a few sites but now I’m sure:
    <u>HTML is stupid!!!</u>
    <center>I can’t believe there’s no way to control the styling of
    my website without doing everything all over again!</center>

.. code-block:: html

    <!-- Recommended -->

    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>My first CSS-only redesign</title>

        <link rel="stylesheet" href="default.css">
    </head>
    <body>
        <h1>My first CSS-only redesign</h1>

        <p>I’ve read about this on a few sites but today I’m actually
          doing it: separating concerns and avoiding anything in the HTML of
          my website that is presentational.</p>
        <p>It’s awesome!</p>
    </body>
    </html>
