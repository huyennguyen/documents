======
Bundle 
======

*******
Naming
*******

1. Always start with Nilead (i.e. NileadMyNewBundle)

2. Always create `composer.json <https://getcomposer.org/>`_

3. Always use `psr-4 <http://www.php-fig.org/psr/psr-4/>`_