+-------------------------------------------+------------------------+
| Interface                                 | Abstract               |
+===========================================+========================+
| Constraint to implement abstract methods  | same                   |
+-------------------------------------------+------------------------+
|                                           | re-usability of code   |
+-------------------------------------------+------------------------+
| multiple implementation is possible       | no                     |
+-------------------------------------------+------------------------+


**Note:**
 - use interface for exposing expected methods (and their possible parameters and results) to the client code
 - use abstract to re-use code and also for the same reasons above
 - remember that it is not possible to extend more than 1 abstract
   
**Interesting links:**
 - `Diamond issue <http://en.wikipedia.org/wiki/Multiple_inheritance#The_diamond_problem>`_
 - `Interface vs Abstract <http://stackoverflow.com/questions/761194/interface-vs-abstract-class-general-oo>`_